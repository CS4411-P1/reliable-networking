/*
 * minithread.c:
 *      This file provides a few function headers for the procedures that
 *      you are required to implement for the minithread assignment.
 *
 *      EXCEPT WHERE NOTED YOUR IMPLEMENTATION MUST CONFORM TO THE
 *      NAMING AND TYPING OF THESE PROCEDURES.
 *
 */
#include <stdlib.h>
#include <stdio.h>
#include "interrupts.h"
#include "minithread.h"
#include "queue.h"
#include "multilevel_queue.h"
#include "synch.h"
#include "alarm.h"
#include <assert.h>
#include "network.h"
#include "miniheader.h"
#include "minimsg.h"
#include "minisocket.h"

/*
 * A minithread should be defined either in this file or in a private
 * header file.  Minithreads have a stack pointer with to make procedure
 * calls, a stackbase which points to the bottom of the procedure
 * call stack, the ability to be enqueueed and dequeued, and any other state
 * that you feel they must have.
 */

#define noop	//No operation
#define NUMLEVELS 4


//Global vars
int nextThreadID;
int quantaInCycle;	//Number of quanta elapsed in this 160-quanta cycle
long long int ticksElapsed = 0;
multilevel_queue_t *readyQueue;
queue_t *zombieQueue;
minithread_t *runningThreadPointer;
minithread_t *reaperThread;
minithread_t *kernelThread;
semaphore_t *waitSem;	//Semaphore for reaper thread
queue_t *alarmQueue;

typedef enum status status_t;

enum status { NEW,
	WAITING,
	RUNNABLE,
	RUNNING,
	SLEEPING,
	ZOMBIE
};

struct minithread {
	stack_pointer_t stacktop;
	stack_pointer_t stackbase;
	int threadID;
	status_t status;
	long long int quanta_used;
};

/* 'Final' cleanup function */
int final_proc(int *threadID){
	runningThreadPointer->status = ZOMBIE;
	set_interrupt_level(DISABLED);
	queue_append(zombieQueue, runningThreadPointer);	//Add to zombie queue
	semaphore_V(waitSem);	//Increments semaphore or makes waiting thread runnable, but no context switch
	minithread_stop();	//Context switch so final_proc never returns
	//minithread_switch ultimately re-enables interrupts
	return 0;
}

int reaper_proc(int *sem){
	while(1){
		if (zombieQueue == NULL){
			noop;
		}	
		else{
			void *current = NULL;
			queue_dequeue(zombieQueue, &current);	//Get first zombie
			while(current != NULL){		//Go through zombie list
				minithread_free_stack(((minithread_t*)current)->stackbase);	//Free zombie TCB stack
				queue_dequeue(zombieQueue, &current);
			}		
		}	//End else
		semaphore_P(waitSem);
	}	//End while
	return 0;
}

/* minithread functions */

minithread_t*
minithread_fork(proc_t proc, arg_t arg) {
	minithread_t* minithreadPointer = minithread_create(proc,arg);
	minithreadPointer->status = RUNNABLE;
	multilevel_queue_enqueue(readyQueue, 0, minithreadPointer);	//Add to runnable queue	
    return minithreadPointer;
}

minithread_t*
minithread_create(proc_t proc, arg_t arg) {
    minithread_t* minithreadPointer = (minithread_t*) malloc(sizeof(minithread_t));
	assert(minithreadPointer != NULL);
	minithread_allocate_stack(&minithreadPointer->stackbase,&minithreadPointer->stacktop);
	minithread_initialize_stack(&minithreadPointer->stacktop, proc, arg, final_proc, NULL);
	minithreadPointer->status = NEW;
	nextThreadID++;		//Thread IDs assigned sequentially
	minithreadPointer->threadID = nextThreadID;
	minithreadPointer->quanta_used = 0;
    return minithreadPointer;
}

minithread_t*
minithread_self() {
    return runningThreadPointer;
}

int
minithread_id() {
    return runningThreadPointer->threadID;
}

/* Return the appropriate queue level for minithread t based on
 * the number of quanta used.
 */
int minithread_get_level(minithread_t *t) {
	if (t->quanta_used < 1){
		return 0;
	}
	else if ((t->quanta_used >= 1) && (t->quanta_used < 3)){
		return 1;
	}
	else if ((t->quanta_used >= 3) && (t->quanta_used < 7)){
		return 2;
	}
	else {
		return 3;
	}
}

/* Block the calling thread and perform a context switch to the next thread
 * from the appropriate level of the multi-level queue.
 */
void
minithread_stop() {
	//figure out level to draw from
	int level;
	set_interrupt_level(DISABLED);
	if (quantaInCycle < 80) {	//Level 0 for 80 quanta
		level = 0;
	}
	else if (quantaInCycle < 120) {	//Level 1 for next 40 quanta. 80+40=120
		level = 1;
	}
	else if (quantaInCycle < 144) { //Level 2 for next 24 quanta. 120+24=144
		level = 2;
	}
	else {	//Level 3 for next 16 quanta, until quantaInCycle resets at 160
		level = 3;
	}
	void *temp = NULL;
	multilevel_queue_dequeue(readyQueue,level,&temp);	//Check ready queue
	if (temp == NULL){	//Ready queue was empty or had lone thread, switch to idle thread
		temp = kernelThread;
	}
	minithread_t *readyThread = (minithread_t*) temp;
	temp = runningThreadPointer;	//Copying pointer for switching
	runningThreadPointer = readyThread;
	readyThread->status = RUNNING;
	minithread_switch(&(((minithread_t*)temp)->stacktop), &(readyThread->stacktop));	//Context switch. temp is old thread
	//minithread_switch re-enables interrupts
}

/* Set the calling thread's status to RUNNABLE and add it to the appropriate
 * queue level based on how many quanta it has consumed so far.
 */
void
minithread_start(minithread_t *t) {
	t->status = RUNNABLE;
	int level = minithread_get_level(t);
	interrupt_level_t old_level = set_interrupt_level(DISABLED);
	multilevel_queue_enqueue(readyQueue, level, t);	//Add to runnable queue
	set_interrupt_level(old_level);
}

void
minithread_yield() {
	runningThreadPointer->quanta_used++;	//Thread used quanta on yield
	minithread_start(runningThreadPointer);	//send stopped thread to back of queue
	minithread_stop();
}

void run_scheduler() {
	runningThreadPointer->quanta_used++;	//update running minithread's quanta_used
	quantaInCycle = (quantaInCycle + 1)%160;
	if (runningThreadPointer != kernelThread){ //Don't add kernel thread to queue
		int level = minithread_get_level(runningThreadPointer);
		if (queue_priority_peek(*(readyQueue->queues + level)) == -1) {
			return;	//Empty queue level, so single thread; don't switch
	}		
		minithread_start(runningThreadPointer);
	}
	minithread_stop();	//Pre-emptively switch
}

/*
 * This is the clock interrupt handling routine.
 * You have to call minithread_clock_init with this
 * function as parameter in minithread_system_initialize
 */
void 
clock_handler(void* arg)
{
	interrupt_level_t old_level = set_interrupt_level(DISABLED);
	ticksElapsed++;	//Increment global ticks counter
	alarm_activate_manager();
	//printf("\nReturned from alarm manager");
	run_scheduler();
	//printf("\nReturned from run_scheduler");
	set_interrupt_level(old_level);
	//printf("\nExiting clock handler.");
}

/*
 * Generalized alarm_handler_t function to wake up minithread 't'
 */
void minithread_wake(void *t){
	minithread_start((minithread_t*)t);
}

/*
 * Alarm handler function to terminate a socket on receiving FIN
 */
void socket_terminator(void* arg){
	printf("\nSocket terminator activated.");
	fflush(stdout);
	minisocket_t* socket = (minisocket_t*)arg;
	int port_num = (socket->isServer == 1) ? (socket->server_port) : (socket->client_port);
	socket->sstatus = CLOSED;
	printf("\nSocket closed");
	free(socket);
	minisocketPtr[port_num] = NULL;
}

/*
 * Check if packet size is not greater than header size or not less than MAX_SIZE
 */
 int packet_outof_limits(network_interrupt_arg_t *arg){
	 if(!(arg->size >= sizeof(mini_header_t)) || !(arg->size <= MAX_NETWORK_PKT_SIZE))
		return 1;
	return 0;
 }
 
 /*
  * Helper function to send a quick ACK
  */
void send_quick_pkt(int port_num, char msg_type){
	mini_header_reliable_t* msg_hdr = (mini_header_reliable_t*) malloc(sizeof(mini_header_reliable_t));
	//return error if malloc fails
	if(msg_hdr == NULL){
		return;
	}
	msg_hdr->protocol = PROTOCOL_MINISTREAM;
	msg_hdr->message_type = msg_type;
	pack_unsigned_int(msg_hdr->seq_number,(minisocketPtr[port_num])->seq_number);
	pack_unsigned_int(msg_hdr->ack_number,(minisocketPtr[port_num])->ack_number);
	network_address_t dest_addr;
	//Initialize header based on who ACKs
	if((minisocketPtr[port_num])->isServer == 1){
		//server ACKs
		pack_address(msg_hdr->source_address,(minisocketPtr[port_num])->server_address);
		pack_unsigned_short(msg_hdr->source_port,(minisocketPtr[port_num])->server_port);
		
		pack_address(msg_hdr->destination_address,(minisocketPtr[port_num])->client_address);
		pack_unsigned_short(msg_hdr->destination_port,(minisocketPtr[port_num])->client_port);
		
		memcpy(dest_addr,(minisocketPtr[port_num])->client_address,sizeof(network_address_t));
		
	}
	else if((minisocketPtr[port_num])->isServer == 0){
		//client ACKs
		pack_address(msg_hdr->source_address,(minisocketPtr[port_num])->client_address);
		pack_unsigned_short(msg_hdr->source_port,(minisocketPtr[port_num])->client_port);
		
		pack_address(msg_hdr->destination_address,(minisocketPtr[port_num])->server_address);
		pack_unsigned_short(msg_hdr->destination_port,(minisocketPtr[port_num])->server_port);
		
		memcpy(dest_addr,(minisocketPtr[port_num])->server_address,sizeof(network_address_t));
	}
	network_send_pkt(dest_addr,sizeof(mini_header_reliable_t),(char*)msg_hdr,0,NULL);					
	free(msg_hdr);
	return;
}

/*
 * Helper function to check if the packet received is from valid connection 
 */
int packet_from_valid_conn(mini_header_reliable_t* arrive_header){
	if (arrive_header == NULL){
		return 0;
	}
	int port_num = unpack_unsigned_short(arrive_header->destination_port);
	network_address_t src_addr;
	unpack_address(arrive_header->source_address, src_addr);
	if ((minisocketPtr[port_num])->isServer == 1) {
		if ((unpack_unsigned_short(arrive_header->source_port) == (minisocketPtr[port_num])->client_port) && (network_compare_network_addresses(src_addr, (minisocketPtr[port_num])->client_address))){
			return 1;;
		}
	}
	else { //Received on client
		if ((unpack_unsigned_short(arrive_header->source_port) == (minisocketPtr[port_num])->server_port) && (network_compare_network_addresses(src_addr, (minisocketPtr[port_num])->server_address))){
			return 1;;
		}
	}
	return 0;
}

/* Extract header and data from packet, queue data to appropriate unbound port,
 * call semaphore_V for that port, then free the packet.
 */
void network_handler(network_interrupt_arg_t *arg){
	interrupt_level_t old_level = set_interrupt_level(DISABLED);
	
	mini_header_t* arrive_header = (mini_header_t*) (arg->buffer);
	int port_num;
	
	//Check protocol
	if(arrive_header->protocol == PROTOCOL_MINIDATAGRAM){
		//Handle UDP case
		
		//Check if packet size is not greater than header size or not less than MAX_SIZE
		if(packet_outof_limits(arg)){
			//Drop packet
			free(arg);
			set_interrupt_level(old_level);
			return;
		}
		//Check if port number  is valid
		port_num = unpack_unsigned_short((arrive_header)->destination_port);
		if((port_num >= 0) && (port_num <= MAX_UNBOUND_PORT_NUM) && miniportPtr[port_num] != NULL){
	
			//Add to queue, allow receive to free packet after processing
			queue_append((miniportPtr[port_num])->port_data.unbound.incoming_data,arg);
			semaphore_V(miniportPtr[port_num]->port_data.unbound.datagrams_ready);
			set_interrupt_level(old_level);
			return;
		}
		else{
			//If not valid, drop packet
			free(arg);
			set_interrupt_level(old_level);
			return;
		}
	//End of UDP handler	
	}
	else if(arrive_header->protocol == PROTOCOL_MINISTREAM){
		//Handle TCP case
		printf("\nProtocol is TCP\n");
		mini_header_reliable_t* arrive_header = (mini_header_reliable_t*) (arg->buffer);
		//Check if packet size is not greater than header size or not less than MAX_SIZE
		if(packet_outof_limits(arg)){
			//Drop packet
			printf("\nPacket dropped. Out of size limits.");
			free(arg);
			set_interrupt_level(old_level);
			return;
		}

		//Check if port number  is valid
		port_num = unpack_unsigned_short((arrive_header)->destination_port);
		if(!((port_num >= 0) && (port_num < NUM_MINISOCKETS) && minisocketPtr[port_num] != NULL)){
			//Drop packet
			printf("\nPacket dropped. Invalid port num.");
			free(arg);
			set_interrupt_level(old_level);
			return;
		}
		
		//If SYN to non-LISTENING socket, reply FIN
		if ((arrive_header->message_type == MSG_SYN) && ((minisocketPtr[port_num])->sstatus != LISTENING)){			
			//Got duplicate or unexpected SYN
			printf("\nGot duplicate or unexpected SYN");
			if (packet_from_valid_conn(arrive_header) != 1){
				//Got a SYN from invalid connection
				send_quick_pkt(port_num, MSG_FIN);
			}
			free(arg);
			set_interrupt_level(old_level);
			return;
		}
		
		//Check if packet has valid packet connection parameters if connected
		if ((arrive_header->message_type != MSG_SYN) && ((minisocketPtr[port_num])->sstatus == CONNECTED) && (packet_from_valid_conn(arrive_header) != 1)){
			free(arg);
			set_interrupt_level(old_level);
			return;
		} 
		//Add to queue, allow server to free packet after processing
		if ((minisocketPtr[port_num])->sstatus != CLOSED){
			printf("\nMinisocket status is not CLOSED. isServer=%d",(minisocketPtr[port_num])->isServer);
			printf("\nSocket seq number = %u, Socket ack number = %u, Msg seq number = %u, Msg ack number = %u, isServer=%d",(minisocketPtr[port_num])->seq_number,(minisocketPtr[port_num])->ack_number , (unpack_unsigned_int(arrive_header->seq_number)), (unpack_unsigned_int(arrive_header->ack_number)),(minisocketPtr[port_num])->isServer);
			if ((minisocketPtr[port_num])->ack_number > unpack_unsigned_int(arrive_header->seq_number)){
				//Drop duplicate packet after sending ACK. Do not send ACK for duplicate MSG ACKS
				
				printf("\nPacket dropped. Duplicate packet. isServer=%d",(minisocketPtr[port_num])->isServer);
				unsigned int msg_len_hdr = ((network_interrupt_arg_t*)arg)->size - sizeof(mini_header_reliable_t);
				if(arrive_header->message_type == MSG_ACK && msg_len_hdr != 0){
					send_quick_pkt(port_num, MSG_ACK);
				}
				else if(arrive_header->message_type == MSG_SYNACK){
					send_quick_pkt(port_num, MSG_ACK);
				}
				free(arg);
				set_interrupt_level(old_level);
				return;
			}
			else{	
				printf("\nNon-duplicate packet.");
				if ((((minisocketPtr[port_num])->seq_number) > (unpack_unsigned_int(arrive_header->ack_number))) && (arrive_header->message_type == MSG_ACK)){
					//Received duplicate message that was already ACKed in the past
					printf("\nDuplicate message packet in handler. isServer=%d",(minisocketPtr[port_num])->isServer);
					fflush(stdout);
					free(arg);
					set_interrupt_level(old_level);
					return;
				} 
				if (arrive_header->message_type == MSG_FIN){
					printf("\nMSG_FIN received (handler)");
					if ((minisocketPtr[port_num])->sstatus != CLOSING){
						printf("\nSocket not CLOSING, accept MSG_FIN");
						(minisocketPtr[port_num])->sstatus = CLOSING;
						(minisocketPtr[port_num])->seq_number = unpack_unsigned_int(arrive_header->ack_number);
						(minisocketPtr[port_num])->ack_number++;
						printf("\nUpdated Socket seq number = %u, Socket ack number = %u, Msg seq number = %u Msg ack number=%u isServer=%d",(minisocketPtr[port_num])->seq_number,(minisocketPtr[port_num])->ack_number , (unpack_unsigned_int(arrive_header->seq_number)),(unpack_unsigned_int(arrive_header->ack_number)),(minisocketPtr[port_num])->isServer);
						send_quick_pkt(port_num, MSG_ACK);
						register_alarm(3000, socket_terminator, minisocketPtr[port_num]);
					}
					free(arg);
					set_interrupt_level(old_level);
					return;
				}
				queue_append((minisocketPtr[port_num])->incoming_data,arg);		
				if (arrive_header->message_type == MSG_SYN){
					printf("\nMSG_SYN received (handler)");
					//Prevent duplicated SYNs
					semaphore_V(minisocketPtr[port_num]->packet_arrive);
					set_interrupt_level(old_level);
					return;
				}
				//Not a SYN, send an ACK
				unsigned int msg_len = ((network_interrupt_arg_t*)arg)->size - sizeof(mini_header_reliable_t);
				(minisocketPtr[port_num])->seq_number = unpack_unsigned_int(arrive_header->ack_number);
				(minisocketPtr[port_num])->ack_number = (minisocketPtr[port_num])->ack_number + msg_len;
				printf("\nMsg length = %d",msg_len);
				printf("\nUpdated Socket seq number = %u, Socket ack number = %u, Msg seq number = %u Msg ack number = %u isServer=%d",(minisocketPtr[port_num])->seq_number,(minisocketPtr[port_num])->ack_number , (unpack_unsigned_int(arrive_header->seq_number)),(unpack_unsigned_int(arrive_header->ack_number)),(minisocketPtr[port_num])->isServer);
			
				
				if (arrive_header->message_type == MSG_SYNACK){
					printf("\nReceived MSG_SYNACK.");
					(minisocketPtr[port_num])->ack_number = 1;
					printf("\nUpdated Socket seq number = %u, Socket ack number = %u, Msg seq number = %u Msg ack no. = %u isServer=%d",(minisocketPtr[port_num])->seq_number,(minisocketPtr[port_num])->ack_number , (unpack_unsigned_int(arrive_header->seq_number)),(unpack_unsigned_int(arrive_header->ack_number)),(minisocketPtr[port_num])->isServer);
				}
				
				//Respond with quick-ACK if non-empty ACK or SYNACK
				if (((arrive_header->message_type == MSG_ACK) && (msg_len > 0)) || (arrive_header->message_type == MSG_SYNACK)){
					printf("\nReplying quick ACK");
					send_quick_pkt(port_num, MSG_ACK);
				}
				
				semaphore_V(minisocketPtr[port_num]->packet_arrive);
				set_interrupt_level(old_level);
				return;
			}
		}
		else{
			printf("\nPacket dropped. Minisocket CLOSED.");
			//Drop packet
			free(arg);
			set_interrupt_level(old_level);
			return;
		}
	
	}
	
}

/*
 * Initialization.
 *
 *      minithread_system_initialize:
 *       This procedure should be called from your C main procedure
 *       to turn a single threaded UNIX process into a multithreaded
 *       program.
 *
 *       Initialize any private data structures.
 *       Create the idle thread.
 *       Fork the thread which should call mainproc(mainarg)
 *       Start scheduling.
 *
 */
void
minithread_system_initialize(proc_t mainproc, arg_t mainarg) {
	//Create ready and zombie queues
	readyQueue = multilevel_queue_new(NUMLEVELS);
	assert(readyQueue != NULL);
	zombieQueue = queue_new();
	assert(zombieQueue != NULL);

	nextThreadID = 0;

	waitSem = semaphore_create();
	semaphore_initialize(waitSem,0);

	//Create alarm queue
	alarmQueue = queue_new();
	assert(alarmQueue != NULL);

	//Create kernel thread TCB
	kernelThread = (minithread_t*) malloc(sizeof(minithread_t));	//Kernel thread does not use minithread_create as we do not allocate stack to it
	kernelThread->stacktop = NULL;
	kernelThread->stackbase = NULL;
	kernelThread->threadID = nextThreadID;
	kernelThread->status = RUNNING;
	kernelThread->quanta_used = 0;
	runningThreadPointer = kernelThread;

	//Create main thread
	minithread_fork(mainproc, mainarg);

	//Create reaper thread
	reaperThread = minithread_fork(reaper_proc,NULL);
	
	//Initialize clock
	ticksElapsed = 0;
	quantaInCycle = -1;	//Quanta is between ticks, first tick is 0 quanta used
	minithread_clock_init(100*MILLISECOND,clock_handler);
	
	//Initialize networking
	minimsg_initialize();
	minisocket_initialize();
	network_initialize(network_handler);

	set_interrupt_level(ENABLED);

	while(1);	//Use kernel thread as idle thread
}

/*
 * sleep with timeout in milliseconds
 */
void 
minithread_sleep_with_timeout(int delay)
{
	set_interrupt_level(DISABLED);	//Re-enabled by minithread_stop
	runningThreadPointer->status = SLEEPING;
	runningThreadPointer->quanta_used++;	//Thread is yielding, uses quanta
	register_alarm(delay, minithread_wake, runningThreadPointer); //Set up alarm
	minithread_stop();
}


