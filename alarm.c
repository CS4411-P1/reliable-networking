#include <stdio.h>
#include <stdlib.h>

#include "interrupts.h"
#include "alarm.h"
#include "minithread.h"
#include "queue.h"

extern long long int ticksElapsed;

typedef struct alarm alarm_t;

struct alarm {
	long long int scheduledFor;	//At which tick to activate
	alarm_handler_t handler;
	void *arg;	//argument associated with the alarm
};

//Sorted queue of alarms. Initialized in minithread.c
extern queue_t *alarmQueue;

/* Search alarm queue for pending alarms and activate them by calling their
 * handler function for their TCB. The queue is sorted, so this doesn't need
 * to look through the entire queue. If no pending alarms, does nothing.
 */
void alarm_activate_manager(){
	while (queue_length(alarmQueue) > 0 && ticksElapsed >= queue_priority_peek(alarmQueue)){
		//Get all alarms that are scheduled to activate
		void *alarm;		
		queue_dequeue(alarmQueue, &alarm);
		if (alarm == NULL){
			return;
		}
        //alarm_t* activeAlarm = (alarm_t*)alarm;
        //printf("\nblah blah");
		printf("\nAlarm scheduled for %lld went off.",((alarm_t*)alarm)->scheduledFor);
		(((alarm_t*)alarm)->handler)(((alarm_t*)alarm)->arg); //Run alarm handler with arg
		//printf("\nReturned from alarm handler");
		free(alarm);
		//printf("\nFreed alarm in alarm manager");
	}
}

/* see alarm.h */
alarm_id
register_alarm(int delay, alarm_handler_t func, void *arg)
{
	interrupt_level_t old_level = set_interrupt_level(DISABLED);
	alarm_t *newAlarm;
    newAlarm = (alarm_t*) malloc(sizeof(alarm_t));
	assert(newAlarm != NULL);
	newAlarm->scheduledFor = ticksElapsed + (delay/100)+1;	//Period is 100 msec. Activates on one tick after due time
	newAlarm->handler = func;	//Alarm handler function
	newAlarm->arg = arg;	
    //Place alarm into queue sorted by due time
	queue_insertSorted(alarmQueue, newAlarm, newAlarm->scheduledFor);
	printf("\nAlarm registered for time %lld",newAlarm->scheduledFor);
    set_interrupt_level(old_level);
    return newAlarm;
}

/* see alarm.h */
int
deregister_alarm(alarm_id alarm)
{
	printf("\nDeregistering alarm %lld",((alarm_t*)alarm)->scheduledFor);
    if (queue_delete(alarmQueue, alarm) == 0) {
    //Alarm was not executed
    return 0;
    }
    return 1;  //Otherwise
}

/*
** vim: ts=4 sw=4 et cindent
*/
