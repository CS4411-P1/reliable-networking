/*
 *	Implementation of minisockets.
 */
#include "minisocket.h"

int client_port_num = MAX_SERVER_SOCKET_NUM+1;	//Used to assign new client port numbers
minisocket_t *minisocketPtr[NUM_MINISOCKETS];	//Array of minisocket pointers
semaphore_t* mutex; 

/* Initializes the minisocket layer. */
void minisocket_initialize()
{
	int i;
	//Set sockets array to NULL
	for (i = 0; i < NUM_MINISOCKETS; i++){
		minisocketPtr[i] = NULL;
	}
	//Create and Initialize mutex
	mutex = semaphore_create();
	semaphore_initialize(mutex,1);
}

void timeout_handler(void* arg){
	//printf("\nInside timeout handler");
	//minisocket_t* socket = (minisocket_t*)arg;
	//printf("\nCast to minisocket");
	((minisocket_t*)arg)->wokenBy = 1;
	//printf("\nSet wokenBy to 1");
	semaphore_V(((minisocket_t*)arg)->packet_arrive);
	//printf("\nSemaphore V called by timeout");
}

/*
 * This send helper function tries to send one fragment to the other end of socket.
 * 
 * The send call should block until the remote host has ACKnowledged receipt of
 * the message.  This does not necessarily imply that the application has called
 * 'minisocket_receive', only that the packet is buffered pending a future
 * receive.
 * 
 * Arguments: the socket on which the communication is made (socket), the
 *            message to be transmitted (msg) and its length (len) and messagetype
 * 
 * Return value: returns the number of successfully transmitted bytes. Sets the
 *               error code and returns -1 if an error is encountered.
 */
int minisocket_send_helper(minisocket_t *socket, const char *msg, int len,char message_type,minisocket_error *error)
{
	//Check for argument sanity
	if(socket == NULL || len < 0 || (len+sizeof(mini_header_reliable_t) > MAX_NETWORK_PKT_SIZE)){
		*error = SOCKET_INVALIDPARAMS;
		printf("\nSend helper got invalid param.");
		return -1;
	}
	
	semaphore_P(socket->mutex);
	
	mini_header_reliable_t* msg_hdr = (mini_header_reliable_t*) malloc(sizeof(mini_header_reliable_t));
    //return error if malloc fails
    if(msg_hdr == NULL){
		*error = SOCKET_OUTOFMEMORY;
		printf("\nSend helper malloc failed.");
		semaphore_V(socket->mutex);
		return -1;
	}
	
	msg_hdr->protocol = PROTOCOL_MINISTREAM;
	msg_hdr->message_type = message_type;
	pack_unsigned_int(msg_hdr->seq_number,socket->seq_number);
	pack_unsigned_int(msg_hdr->ack_number,socket->ack_number);
	
	network_address_t dest_addr;
	//Initialize header based on who called send
    if(socket->isServer == 1){
		//server called send 
		pack_address(msg_hdr->source_address,socket->server_address);
		pack_unsigned_short(msg_hdr->source_port,socket->server_port);
		
		pack_address(msg_hdr->destination_address,socket->client_address);
		pack_unsigned_short(msg_hdr->destination_port,socket->client_port);
		
		memcpy(dest_addr,socket->client_address,sizeof(network_address_t));
		
	}
	else if(socket->isServer == 0){
		//client called send
		pack_address(msg_hdr->source_address,socket->client_address);
		pack_unsigned_short(msg_hdr->source_port,socket->client_port);
		
		pack_address(msg_hdr->destination_address,socket->server_address);
		pack_unsigned_short(msg_hdr->destination_port,socket->server_port);
		
		memcpy(dest_addr,socket->server_address,sizeof(network_address_t));
	}
	
	int num_tries = 0;
	alarm_id aid;
	int old_seq_num = socket->seq_number;
	int len_sent = 0;
		
	do{	
		if (socket->sstatus == CLOSING){
			//Socket received FIN and went to CLOSING state
			printf("\nAbort send helper as we received a FIN");
			free(msg_hdr);
			*error = SOCKET_SENDERROR;
			semaphore_V(socket->mutex);
			return -1;
		}
		//send message
		if(network_send_pkt(dest_addr,sizeof(mini_header_reliable_t),(char*)msg_hdr,len,msg) == -1) {
			//Failed to send data
			printf("\nnetwork_send_pkt failed.");
			free(msg_hdr);
			*error = SOCKET_SENDERROR;
			semaphore_V(socket->mutex);
			return -1;
		}
		
		//Register alarm and block
		aid = register_alarm(INITIAL_TIMEOUT<<num_tries++,timeout_handler,socket);
		printf("\nAlarm registered for %d th try. isServer=%d",num_tries,socket->isServer);
		printf("\nisServer=%d blocked in send helper",socket->isServer);
		semaphore_P(socket->packet_arrive);
		
		//if not woken up by alarm
		if(socket->wokenBy != 1){
				//woken by packet arrival
				printf("\nWoken up by packet arrival. isServer=%d",socket->isServer);
				//deque the packet 
				void * cur_arg;
				queue_dequeue(socket->incoming_data,&cur_arg);
				mini_header_reliable_t* cur_header = (mini_header_reliable_t*)(((network_interrupt_arg_t*)cur_arg)->buffer);
				
				unsigned int msg_len = ((network_interrupt_arg_t*)cur_arg)->size - sizeof(mini_header_reliable_t);
				
				printf("\nSocket seq number = %u, Socket ack number = %u, Msg seq number = %u  Msg ack number = %u isServer=%d in send helper",socket->seq_number,socket->ack_number , (unpack_unsigned_int(cur_header->seq_number)), (unpack_unsigned_int(cur_header->ack_number)),socket->isServer);

				if(cur_header->message_type == MSG_ACK){
					//Received non-duplicate ACK (guaranteed by network handler)
					printf("\nReceived a non-duplicate ACK");
					deregister_alarm(aid);
					//Calculate length of message transmitted
					len_sent = unpack_unsigned_int(cur_header->ack_number) - old_seq_num;
					printf("\nSend helper sent %d bytes",len_sent);
					if(msg_len == 0){
						//Empty ACK
						free(cur_arg);
						break;
					}
					else{
						queue_prepend(socket->incoming_data,cur_arg);
						semaphore_V(socket->packet_arrive);	//Do not block future receivers
						break;
					}	
				}
				else if(message_type == MSG_SYN){
					if (cur_header->message_type == MSG_FIN){
						printf("\nGot a FIN in response to a SYN");
						deregister_alarm(aid);
						free(cur_arg);
						*error = SOCKET_BUSY;
						socket->wokenBy = 0;
						free(msg_hdr);
						semaphore_V(socket->mutex);
						return -1;
					}
					else if (cur_header->message_type == MSG_SYNACK){
						printf("\nGot a SYNACK (send helper)");
						deregister_alarm(aid);
						free(cur_arg);
						break;
					}
				}
				
		}
		socket->wokenBy = 0;
		printf("\nWoken up by alarm. isServer = %d",socket->isServer);
    }while(num_tries < MAX_TRIES);
    
    if(num_tries >= MAX_TRIES){
		//retries exceeded
		printf("\nSend helper timed out.");		
		free(msg_hdr);
		*error = SOCKET_SENDERROR;
		semaphore_V(socket->mutex);
		return -1;
	}
    printf("\nReliably sent len = %d MSG type %d by isServer=%d",len,message_type,socket->isServer);
    free(msg_hdr);
    *error = SOCKET_NOERROR;
    semaphore_V(socket->mutex);
    return len_sent;	
}

/*
 * Listen for a connection from somebody else. When communication link is
 * created return a minisocket_t through which the communication can be made
 * from now on.
 *
 * The argument "port" is the port number on the local machine to which the
 * client will connect.
 *
 * Return value: the minisocket_t created, otherwise NULL with the errorcode
 * stored in the "error" variable.
 */
minisocket_t* minisocket_server_create(int port, minisocket_error *error)
{
    //Check for port number sanity
    if(!(port>=0 && port<=MAX_SERVER_SOCKET_NUM)){
		*error = SOCKET_INVALIDPARAMS;
    	return NULL;
    }
    interrupt_level_t old_level = set_interrupt_level(DISABLED);
    //If port is in use 
    if (minisocketPtr[port] != NULL){
		if (minisocketPtr[port]->sstatus != CLOSED){
			*error = SOCKET_PORTINUSE;
			set_interrupt_level(old_level);
			return NULL;
		}
	}
	minisocket_t *new_server_port;
	if (minisocketPtr[port] == NULL){			
		new_server_port = (minisocket_t*) malloc(sizeof(minisocket_t));
		//Check for malloc error 
		if(new_server_port == NULL){
			*error = SOCKET_OUTOFMEMORY;
			set_interrupt_level(old_level);
			return NULL;
		}
	}
	else {
		//Socket exists, but CLOSED
		new_server_port = minisocketPtr[port];
	}
	//Initialize socket
	new_server_port->server_port = port;
	network_get_my_address(new_server_port->server_address);
	new_server_port->incoming_data = queue_new();
	new_server_port->packet_arrive = semaphore_create();
	new_server_port->mutex = semaphore_create();
	new_server_port->isServer = 1;
	new_server_port->seq_number = 0;
	new_server_port->ack_number = 0;
	new_server_port->wokenBy = 0;
	semaphore_initialize(new_server_port->packet_arrive,0);
	semaphore_initialize(new_server_port->mutex,1);
	
	minisocketPtr[port] = new_server_port;
	
	mini_header_reliable_t* cur_header;
	int len;
	do{
		new_server_port->sstatus = LISTENING;
		new_server_port->seq_number = 0;
		new_server_port->ack_number = 0;
		//Wait for connection SYN request
		do{
			printf("\nServer blocked in create");
			semaphore_P(new_server_port->packet_arrive);
			void * cur_arg;
			queue_dequeue(new_server_port->incoming_data,&cur_arg);
			cur_header = (mini_header_reliable_t*)(((network_interrupt_arg_t*)cur_arg)->buffer);
			if(cur_header->message_type != MSG_SYN){
				free(cur_arg);
			}
		}while(cur_header->message_type != MSG_SYN);
		
		//Got a SYN, stop LISTENING. Trying to connect
		//new_server_port->sstatus = CONNECTING;
		new_server_port->client_port = unpack_unsigned_short(cur_header->source_port);
		unpack_address(cur_header->source_address, new_server_port->client_address);
		new_server_port->ack_number = 1;
		
		//Send MSG_SYNACK, if timeout reset port
		minisocket_error error;
		len = minisocket_send_helper(new_server_port,NULL,0,MSG_SYNACK,&error);
		new_server_port->sstatus = CONNECTED;
		 
	}while(len == -1);//error != SOCKET_NOERROR);
	
	//NOW CONNECTION HAS BEEN ESTABLISHED, and send and receive can be executed
	
	set_interrupt_level(old_level);
	return new_server_port;	
}

/*
 * Initiate the communication with a remote site. When communication is
 * established create a minisocket through which the communication can be made
 * from now on.
 *
 * The first argument is the network address of the remote machine. 
 *
 * The argument "port" is the port number on the remote machine to which the
 * connection is made. The port number of the local machine is one of the free
 * port numbers.
 *
 * Return value: the minisocket_t created, otherwise NULL with the errorcode
 * stored in the "error" variable.
 */
minisocket_t* minisocket_client_create(const network_address_t addr, int port, minisocket_error *error)
{
	int new_port_number;
    //Check for argument sanity
    if(!(port>=0 && port<=MAX_SERVER_SOCKET_NUM) || addr == NULL){
		*error = SOCKET_INVALIDPARAMS;
    	return NULL;
    }
    interrupt_level_t old_level = set_interrupt_level(DISABLED);
    printf("\nClient blocked on create mutex.");
	semaphore_P(mutex);
	printf("\nClient unblocked on create mutex.");
	//If client_port_num hasn't wrapped around, use next port without search
    if (client_port_num < NUM_MINISOCKETS){
    	new_port_number = client_port_num++;
    }
    else{
		//after client_port_num wraps, search for available port
    	for (new_port_number = MAX_SERVER_SOCKET_NUM+1; new_port_number < NUM_MINISOCKETS; new_port_number++){
    		if (minisocketPtr[new_port_number] == NULL || (minisocketPtr[new_port_number])->sstatus == CLOSED){
    			break;
    		}
    	}
    	//if no port is available throw error
    	if (new_port_number == NUM_MINISOCKETS){
			*error= SOCKET_NOMOREPORTS;
			semaphore_V(mutex);
			set_interrupt_level(old_level);
    		return NULL;
    	}
    }
    minisocket_t *new_client_port;
    if (minisocketPtr[new_port_number] == NULL){
		new_client_port = (minisocket_t*) malloc(sizeof(minisocket_t));
    	//Check for malloc error 
    	if(new_client_port == NULL){
			*error = SOCKET_OUTOFMEMORY;
			semaphore_V(mutex);
			set_interrupt_level(old_level);
			return NULL;
		}
	}
	else{
		new_client_port = minisocketPtr[new_port_number];
	}
	//Initialize socket
	new_client_port->server_port = port;
	new_client_port->client_port = new_port_number;
	network_get_my_address(new_client_port->client_address);
	memcpy(new_client_port->server_address,addr,sizeof(network_address_t));
	new_client_port->incoming_data = queue_new();
	new_client_port->packet_arrive = semaphore_create();
	new_client_port->mutex = semaphore_create();
	new_client_port->isServer = 0;
	new_client_port->seq_number = 0;
	new_client_port->ack_number = 0;
	new_client_port->wokenBy = 0;
	semaphore_initialize(new_client_port->packet_arrive,0);
	semaphore_initialize(new_client_port->mutex,1);
	
	minisocketPtr[new_port_number] = new_client_port;
		
	//Send a syn, wait for SYNACK
	new_client_port->sstatus = CONNECTING;
	minisocket_send_helper(new_client_port,NULL,0,MSG_SYN,error);
	if(*error == SOCKET_NOERROR){
		new_client_port->sstatus = CONNECTED;		
		//Network handler sent a quick empty ACK
		semaphore_V(mutex);
		set_interrupt_level(old_level);
		return new_client_port;
	} 
	//Failure
	*error = SOCKET_NOSERVER;
	free(new_client_port);
	semaphore_V(mutex);
	set_interrupt_level(old_level);
    return NULL;
}

/*
 * Send a message to the other end of the socket.
 *
 * The send call should block until the remote host has ACKnowledged receipt of
 * the message.  This does not necessarily imply that the application has called
 * 'minisocket_receive', only that the packet is buffered pending a future
 * receive.
 *
 * It is expected that the order of calls to 'minisocket_send' implies the order
 * in which the concatenated messages will be received.
 *
 * 'minisocket_send' should block until the whole message is reliably
 * transmitted or an error/timeout occurs
 *
 * Arguments: the socket on which the communication is made (socket), the
 *            message to be transmitted (msg) and its length (len).
 * Return value: returns the number of successfully transmitted bytes. Sets the
 *               error code and returns -1 if an error is encountered.
 */
int minisocket_send(minisocket_t *socket, const char *msg, int len, minisocket_error *error)
{
    if (socket == NULL || msg == NULL || len < 0){
		*error = SOCKET_INVALIDPARAMS;
		return -1;
	}
	if (socket->sstatus == CLOSING || socket->sstatus == CLOSED){
		*error = SOCKET_SENDERROR;
		return -1;
	}
    int len_sent = 0;
    int to_send = 0;
    int got_sent = 0;
    while (len_sent < len) {
		to_send = ((len - len_sent) < (MAX_NETWORK_PKT_SIZE - sizeof(mini_header_reliable_t))) ? (len - len_sent) : (MAX_NETWORK_PKT_SIZE - sizeof(mini_header_reliable_t));
		got_sent = minisocket_send_helper(socket, msg + len_sent, to_send, MSG_ACK, error);
		if(got_sent == -1){	//Briefly changing to <= rather than ==
			*error = SOCKET_SENDERROR;
			return len_sent;
		}
		else{
			len_sent += got_sent;
		}
	}
	*error = SOCKET_NOERROR;
    return len_sent;
}

/*
 * Receive a message from the other end of the socket. Blocks until max_len
 * bytes or a full message is received (which can be smaller than max_len
 * bytes).
 *
 * Arguments: the socket on which the communication is made (socket), the memory
 *            location where the received message is returned (msg) and its
 *            maximum length (max_len).
 * Return value: -1 in case of error and sets the error code, the number of
 *           bytes received otherwise
 */
int minisocket_receive(minisocket_t *socket, char *msg, int max_len, minisocket_error *error)
{	
    if (socket == NULL || msg == NULL || max_len <= 0){
		*error = SOCKET_INVALIDPARAMS;
		return -1;
	}
	if (socket->sstatus == CLOSING || socket->sstatus == CLOSED){
		*error = SOCKET_RECEIVEERROR;
		return -1;
	}
	semaphore_P(socket->mutex);
	printf("\nisServer=%d blocked in receive",socket->isServer);
	semaphore_P(socket->packet_arrive);
	void * cur_arg;
	queue_dequeue(socket->incoming_data,&cur_arg);
	network_interrupt_arg_t *packet = (network_interrupt_arg_t*)cur_arg;
	mini_header_reliable_t *cur_header = (mini_header_reliable_t*)(packet->buffer);
	unsigned int msg_len = packet->size - sizeof(mini_header_reliable_t);
	if((cur_header->message_type == MSG_ACK) && (msg_len > 0)){		
		//Extract data from arg
		//Check if data length exceeds max_len. If so, trim at max_len
		if (msg_len > max_len) {
			memcpy(msg, packet->buffer + sizeof(mini_header_reliable_t),max_len);
			//Reassemble packet with remaining bytes
			memcpy(packet->buffer + sizeof(mini_header_reliable_t), packet->buffer + sizeof(mini_header_reliable_t) + max_len, msg_len - max_len);
			free(packet->buffer + sizeof(mini_header_reliable_t) + msg_len - max_len);
			queue_prepend(socket->incoming_data, cur_arg);
			semaphore_V(socket->packet_arrive);
			*error = SOCKET_NOERROR;
			semaphore_V(socket->mutex);
			return max_len;
		}
		else {
			memcpy(msg, packet->buffer + sizeof(mini_header_reliable_t), msg_len);
			free(cur_arg);
			*error = SOCKET_NOERROR;
			semaphore_V(socket->mutex);
			return msg_len;
		}
	}
	*error = SOCKET_RECEIVEERROR;
	semaphore_V(socket->mutex);
	return -1;
}

/* Close a connection. If minisocket_close is issued, any send or receive should
 * fail.  As soon as the other side knows about the close, it should fail any
 * send or receive in progress. The minisocket is destroyed by minisocket_close
 * function.  The function should never fail.
 */
void minisocket_close(minisocket_t *socket)
{	
   if (socket == NULL){
	   return;
   }
   if (socket->sstatus != CLOSING){
	   printf("\nClose initiated by isServer = %d",socket->isServer);
	   socket->sstatus = CLOSING;
	   // Send MSG_FIN using send helper. Close on ACK or timeout
	   minisocket_error error;
	   minisocket_send_helper(socket, NULL, 0, MSG_FIN, &error);
	   int port_num = (socket->isServer == 1) ? (socket->server_port) : (socket->client_port);
	   printf("\nReturned from send helper in close");
	   socket->sstatus = CLOSED;
	   printf("\nSocket status is CLOSED. Freeing socket.");
	   free(socket);
	   printf("\nSocket closed.");
	   minisocketPtr[port_num] = NULL;
	}
}
