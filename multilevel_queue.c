/*
 * Multilevel queue manipulation functions  
 */
#include "multilevel_queue.h"
#include "queue.h"
#include <stdlib.h>
#include <stdio.h>


/*
 * Returns an empty multilevel queue with number_of_levels levels.
 * Returns NULL on error.
 */
multilevel_queue_t* multilevel_queue_new(int number_of_levels)
{
	int i = 0;
	//Initialize multilevel_queue
	multilevel_queue_t* multi_queue = (multilevel_queue_t*)malloc(sizeof(multilevel_queue_t));
	//if malloc fails
	if(multi_queue == NULL)
		return NULL;
	multi_queue->num_levels = number_of_levels;
	multi_queue->queues = (queue_t**)malloc(number_of_levels*sizeof(queue_t*));
	//create number_of_levels queues
	for(i = 0; i< number_of_levels;i++){
		*((multi_queue->queues)+i) = queue_new();
		//If new queue could not be created
		if(*((multi_queue->queues)+i) == NULL)
			return NULL;
	}
	
	return multi_queue;
}

/*
 * Appends a void* to the multilevel queue at the specified level.
 * Return 0 (success) or -1 (failure).
 */
int multilevel_queue_enqueue(multilevel_queue_t* queue, int level, void* item)
{
	//check for NULL pointers
	if(queue == NULL || item == NULL)
		return -1;
	//Fetch pointer to current level queue
	queue_t* cur_queue = *((queue->queues)+level);
	//if queue append fails
	if(queue_append(cur_queue,item) == -1)
		return -1;
	return 0;
}

/*
 * Dequeue and return the first void* from the multilevel queue starting at the specified level. 
 * Levels wrap around so as long as there is something in the multilevel queue an item should be returned.
 * Return the level that the item was located on and that item.
 * If the multilevel queue is empty, return -1 (failure) with a NULL item.
 */
int multilevel_queue_dequeue(multilevel_queue_t* queue, int level, void** item)
{
	int i=0;
	int cur_level;
	//Check for NULL pointers
	if(queue == NULL || item == NULL){
		*item = NULL;
		return -1;
	}
	//Loop to find first non-empty queue
	for(i = 0; i < queue->num_levels; i++){
			//Fetch queue
			cur_level = (level+i)%(queue->num_levels);
			if(queue_length(*((queue->queues)+cur_level)) > 0)
				break;
	}
	//Check if all levels were empty
	if(i == queue->num_levels){
		*item = NULL;
		return -1;
	}
	//if deque fails return -1 and *item is NULL
	if(queue_dequeue(*((queue->queues)+cur_level),item) == -1)
		return -1;
	return cur_level;
}

/* 
 * Free the queue and return 0 (success) or -1 (failure).
 * Do not free the queue nodes; this is the responsibility of the programmer.
 */
int multilevel_queue_free(multilevel_queue_t* queue)
{
	int i = 0;
	//Cannot free NULL queue
	if(queue == NULL)
		return -1;
	//Check if all levels are empty
	for(i = 0; i < queue->num_levels; i++){
		//Cannot free if any level is not empty
		if(queue_length(*((queue->queues)+i)) > 0)
			return -1;
	}
	free(queue);
	return 0;
}
