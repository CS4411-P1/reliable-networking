/*
 *  Implementation of minimsgs and miniports.
 */
#include<stdio.h>
#include<stdlib.h>

#include "minimsg.h"
#include "miniheader.h"
#include "interrupts.h"
#include "network.h"


int bound_port_num = MAX_UNBOUND_PORT_NUM+1;	//Used to assign new bound port numbers
miniport_t *miniportPtr[NUM_MINIPORTS];	//Array of unbound miniport pointers
semaphore_t* mutex; 

/* performs any required initialization of the minimsg layer.  */
void
minimsg_initialize()
{
	int i;
	//Set ports array to NULL
	for (i = 0; i < NUM_MINIPORTS; i++){
		miniportPtr[i] = NULL;
	}
	//Create and Initialize mutex
	mutex = semaphore_create();
	semaphore_initialize(mutex,1);
}

/* Creates an unbound port for listening. Multiple requests to create the same
 * unbound port should return the same miniport reference. It is the responsibility
 * of the programmer to make sure he does not destroy unbound miniports while they
 * are still in use by other threads -- this would result in undefined behavior.
 * Unbound ports must range from 0 to 32767. If the programmer specifies a port number
 * outside this range, it is considered an error.
 */
miniport_t*
miniport_create_unbound(int port_number)
{
	//Check for port number sanity
    if(!(port_number>=0 && port_number<=32767)){
    	return NULL;
    }
    interrupt_level_t old_level = set_interrupt_level(DISABLED);
    //If port is not created already, create one
    if (miniportPtr[port_number] == NULL){
    	miniport_t *newUnboundMiniport = (miniport_t*) malloc(sizeof(miniport_t));
    	//Check for malloc error 
    	if(newUnboundMiniport == NULL)
			return NULL;
		//Initialize port
    	newUnboundMiniport->port_type = 'u';
    	newUnboundMiniport->port_number = port_number;
    	newUnboundMiniport->port_data.unbound.incoming_data = queue_new();
    	newUnboundMiniport->port_data.unbound.datagrams_ready = semaphore_create();
    	semaphore_initialize(newUnboundMiniport->port_data.unbound.datagrams_ready,0);
    	miniportPtr[port_number] = newUnboundMiniport;
	}
    set_interrupt_level(old_level);
    return miniportPtr[port_number];
}

/* Creates a bound port for use in sending packets. The two parameters, addr and
 * remote_unbound_port_number together specify the remote's listening endpoint.
 * This function should assign bound port numbers incrementally between the range
 * 32768 to 65535. Port numbers should not be reused even if they have been destroyed,
 * unless an overflow occurs (ie. going over the 65535 limit) in which case you should
 * wrap around to 32768 again, incrementally assigning port numbers that are not
 * currently in use.
 */
miniport_t*
miniport_create_bound(const network_address_t addr, int remote_unbound_port_number)
{
    int new_port_number;
    //Check for invalid port numbers
    if (remote_unbound_port_number < 0 || remote_unbound_port_number > MAX_UNBOUND_PORT_NUM){
    	return NULL;
    }
    semaphore_P(mutex);
    //If bound_port_num hasn't wrapped around, use next port without search
    if (bound_port_num < NUM_MINIPORTS){
    	new_port_number = bound_port_num++;
    }
    else{
		//after bound_port_num wraps, search for available port
    	for (new_port_number = MAX_UNBOUND_PORT_NUM+1; new_port_number < NUM_MINIPORTS; new_port_number++){
    		if (miniportPtr[new_port_number] == NULL){
    			break;
    		}
    	}
    	//if no port is available throw error
    	if (new_port_number == NUM_MINIPORTS){
    		return NULL;
    	}
    }

    miniport_t *newBoundMiniport = (miniport_t*) malloc(sizeof(miniport_t));
    //return null if malloc fails
    if(newBoundMiniport == NULL)
		return NULL;
	//Initialize port
    newBoundMiniport->port_type = 'b';
    newBoundMiniport->port_number = new_port_number;
    memcpy(newBoundMiniport->port_data.bound.remote_addr,addr,sizeof(network_address_t));
    newBoundMiniport->port_data.bound.remote_unbound_port = remote_unbound_port_number;
    miniportPtr[new_port_number] = newBoundMiniport;
    semaphore_V(mutex);
    return newBoundMiniport;
}

/* Destroys a miniport and frees up its resources. If the miniport was in use at
 * the time it was destroyed, subsequent behavior is undefined.
 */
void
miniport_destroy(miniport_t* miniport)
{
	if (miniport == NULL){
		return;
	}
	if (miniport->port_type == 'u'){
		//Unbound miniport
		queue_free(miniport->port_data.unbound.incoming_data);
		semaphore_destroy(miniport->port_data.unbound.datagrams_ready);
	}
	miniportPtr[miniport->port_number] = NULL;
	free(miniport);
}

/* Sends a message through a locally bound port (the bound port already has an associated
 * receiver address so it is sufficient to just supply the bound port number). In order
 * for the remote system to correctly create a bound port for replies back to the sending
 * system, it needs to know the sender's listening port (specified by local_unbound_port).
 * The msg parameter is a pointer to a data payload that the user wishes to send and does not
 * include a network header; your implementation of minimsg_send must construct the header
 * before calling network_send_pkt(). The return value of this function is the number of
 * data payload bytes sent not inclusive of the header.
 */
int
minimsg_send(miniport_t* local_unbound_port, const miniport_t* local_bound_port, const char* msg, int len)
{
    network_address_t my_address;
    //Check argument sanity
    if (local_unbound_port == NULL || local_bound_port == NULL || msg == NULL || len > MINIMSG_MAX_MSG_SIZE){
    	return -1;
    }

    //construct header, get hdr_len
    mini_header_t *msg_hdr = (mini_header_t*) malloc(sizeof(mini_header_t));
    //return error if malloc fails
    if(msg_hdr == NULL)
		return -1;
    msg_hdr->protocol = PROTOCOL_MINIDATAGRAM;
    network_get_my_address(my_address);
    pack_address(msg_hdr->source_address,my_address);
    pack_unsigned_short(msg_hdr->source_port,local_unbound_port->port_number);
    pack_address(msg_hdr->destination_address,local_bound_port->port_data.bound.remote_addr);
    pack_unsigned_short(msg_hdr->destination_port,local_bound_port->port_data.bound.remote_unbound_port);

    //call network_send_pkt()
    if(network_send_pkt(local_bound_port->port_data.bound.remote_addr,sizeof(mini_header_t),(char*)msg_hdr,len,(char*)msg) == -1) {
    	//Failed to send data
		free(msg_hdr);
    	return -1;
    }
    //free message header
    free(msg_hdr);
    //return no. of data payload bytes sent excluding header
    return len;
}

/* Receives a message through a locally unbound port. Threads that call this function are
 * blocked until a message arrives. Upon arrival of each message, the function must create
 * a new bound port that targets the sender's address and listening port, so that use of
 * this created bound port results in replying directly back to the sender. It is the
 * responsibility of this function to strip off and parse the header before returning the
 * data payload and data length via the respective msg and len parameter. The return value
 * of this function is the number of data payload bytes received not inclusive of the header.
 */

int minimsg_receive(miniport_t* local_unbound_port, miniport_t** new_local_bound_port, char* msg, int *len)
{
	//Check if argument is valid
    if (local_unbound_port == NULL || local_unbound_port->port_number > MAX_UNBOUND_PORT_NUM){
    	return -1;	
    }
    //add thread to unbound port waitQ
    semaphore_P(local_unbound_port->port_data.unbound.datagrams_ready);
    //If message arrives:
    void* packet_from_queue = NULL;
    int hdr_size = sizeof(mini_header_t);
    if (queue_dequeue(local_unbound_port->port_data.unbound.incoming_data, &packet_from_queue) == -1) {
    	return -1;	//Dequeue error
    }
    
    //Extract message and set len
    network_interrupt_arg_t *packet = (network_interrupt_arg_t*) packet_from_queue;
    mini_header_t* header = (mini_header_t*) (packet->buffer);
	memcpy(msg, packet->buffer + hdr_size, packet->size - hdr_size);
	*len = packet->size - hdr_size;
	
	//Create bound port
	int remote_unbound_port = unpack_unsigned_short((header)->source_port);
	network_address_t remote_addr;
	unpack_address((header)->source_address, remote_addr);
	
    if ((*new_local_bound_port = miniport_create_bound(remote_addr, remote_unbound_port)) == NULL){
    	return -1;//Bound port creation error
    }
    
    //Free packet
    free(packet_from_queue);
	  
    return packet->size - hdr_size;
}
